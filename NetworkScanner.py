import datetime
import gettext
from socket import SOCK_STREAM, socket, AF_INET
import threading
import time
from SocketResponseCodes import SocketResponseCodes
from colorama import init, Style, Fore

_ = gettext.gettext
init()


class PortScan(object):
    def __init__(self):  # not right at all!, just used currently for testing. Fix this.
        # if network_segment is None:
        self.network_segment = "192.168.1."  # make it better
        # if ouput_to_console is None:
        self.output_to_console = True
        # if output_type is None:
        self.report_name = "csv"
        response_class = SocketResponseCodes
        self.response_code = response_class.response_code
        self.port_range = (1, 65535)  # 1024
        self.results = []
        self.max_threads = 450
        self.screen_lock = threading.Semaphore(value=1)

    def send_email(self):
        if self.report_name is not None:
            print('Email not yet implemented')

    def write_csv_report(self):
        csv_report = open(self.report_name, 'w+')
        csv_report.write('Host Address, Port, Port Status')
        for result in self.results:
            csv_report.write('{0},{1},{2}'.format(result[0], result[1], result[2]))
        csv_report.close()

    def write_html_report(self):
        html_header = '<html>\n' \
                      '\t<head>\n' \
                      '\t\t<title>Network Scan Results</title>\n' \
                      '\t</head>\n' \
                      '\t<body>\n' \
                      '\t\t<table>\n' \
                      '\t\t\t<th>\n' \
                      '\t\t\t\t\t<td>Host Address</td><td>Port</td><td>Port Status</td>\n' \
                      '\t\t\t</th>'
        html_footer = '\t\t</table>\n' \
                      '\t</body>\n' \
                      '</html>'
        html_report = open(self.report_name, 'w+')
        html_report.write(html_header)
        for result in self.results:
            html_report.write('<tr><td>{0}</td><td>{1}</td><td>{2}</td>'.format(result[0], result[1], result[2]))
        html_report.write(html_footer)
        html_report.close()

    def write_xml_report(self):
        print('XML not yet implemented')
        html_report = open(self.report_name, 'w+')
        # write to file etc.
        html_report.close()

    def generate_report(self):  # code it better
        valid_report_format = False
        if ".csv" in self.report_name and len(self.report_name) > 3:
            valid_report_format = True
            self.write_csv_report()
        elif ".html" in self.report_name and len(self.report_name) > 3:
            valid_report_format = True
            self.write_html_report()
        elif ".xml" in self.report_name and len(self.report_name) > 3:
            valid_report_format = True
            self.write_xml_report()
        if not valid_report_format:
            self.report_name = None
        # results.append(_("Address, Port, Status"))

    def scan_port(self, current_socket):
        tcp_socket = socket(AF_INET, SOCK_STREAM)
        try:
            tcp_socket.settimeout(0.5)  # 0.2 - 0.5 local 3 for remote?
            response = tcp_socket.connect_ex(current_socket)
            self.results.append((current_socket[0], current_socket[1], self.response_code[response]))
            if response == 0:
                self.screen_lock.acquire()
                print'[*] {0} {1}: {2}{3}{4}'.\
                    format(_("Port"), current_socket[1], Fore.GREEN, self.response_code[response], Style.RESET_ALL)
                self.screen_lock.release()
        finally:
            tcp_socket.close()

    def scan_network(self):
        start_time = datetime.datetime.now()
        for host in xrange(1, 255, 1):
            current_host = self.network_segment + str(host)
            print(_('\nHost: {0}\nPorts: {1} - {2}')).format(current_host, self.port_range[0], self.port_range[1])
            for port in xrange(self.port_range[0], self.port_range[1], 1):
                current_socket = (current_host, port)
                threading.Thread(target=self.scan_port, args=(current_socket, 1)).start()
                active_threads = threading.active_count()
                while active_threads > self.max_threads:  # 850 is ~max, but very overloaded.
                    time.sleep(0.1)
                    active_threads = threading.active_count()
        total_time = datetime.datetime.now() - start_time
        print (_('\nTotal time taken to scan network: {0}')).format(total_time)
