from NetworkScanner import PortScan
import gettext
import sys

_ = gettext.gettext

def header(message):
    print("\n"+"-" * len(message))
    print(message)
    print("-" * len(message))

def main():
    header(_('Setup Network Scan'))
    sys.stdout.flush()
    network_segment = raw_input(_('\nNetwork Segment'
                                  '\n(eg. 192.1.1.0/24): '))
    sys.stdout.flush()
    port_range = (raw_input(_('\nPort Range'
                              '\n(eg. 1, 1023): ')))
    sys.stdout.flush()
    timeout = raw_input(_('\nTimeout'
                          '\n(default 0.5): '))
    sys.stdout.flush()
    report_name = raw_input(_('\nReport Name - avaliable formats: .csv, .xml, .html, none (default)'
                              '\n (eg. name.csv): ').lower())
    sys.stdout.flush()
    email_recipient = raw_input(_('\nSend recipient'
                                  '\n(default is None): '))
    sys.stdout.flush()
    header(_('Starting Network Scan'))

    port_scanner = PortScan()
    results = port_scanner.scan_network()  # network_segment,network_subnet,port_range,timeout)
    port_scanner.generate_report(results, report_name)
    port_scanner.send_email(email_recipient,report_name)

if __name__ == '__main__':
    main()